<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests;

use XpertSelect\JsonApi\Serializer\BaseSerializer;

/**
 * @internal
 */
trait HasSerializers
{
    public function simpleSerializer(): BaseSerializer
    {
        return new class () extends BaseSerializer {
            public function getType(): string
            {
                return 'post';
            }

            public function getAttributes(array $model): array
            {
                return ['foo' => $model['foo']];
            }

            public function getIdentifier(array $model): string
            {
                return $model['id'];
            }
        };
    }
}
