<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Serializer;

use RuntimeException;
use Tests\TestCase;
use XpertSelect\JsonApi\Serializer\BaseSerializer;

/**
 * @internal
 */
final class BaseSerializerTest extends TestCase
{
    private $mock;

    public function setUp(): void
    {
        $this->mock = new class () extends BaseSerializer {
            public function getType(): string
            {
                return 'object';
            }

            public function getAttributes(array $model): array
            {
                return [
                    'foo' => $model['foo'],
                    'bar' => $model['bar'],
                ];
            }

            public function getIdentifier(array $model): string
            {
                return $model['id'];
            }
        };
    }

    public function testWithObject(): void
    {
        $model = new ModelTest('1', 'foo');

        $resource = $this->mock->fromObject($model);

        $expected = [
            'id'         => '1',
            'type'       => 'object',
            'attributes' => [
                'foo' => 'foo',
                'bar' => 'bar',
            ],
        ];
        $this->assertEquals($expected, $resource->toArray());
    }

    public function testWithIncorrectObject(): void
    {
        $this->expectException(RuntimeException::class);
        $model = new WrongModelTest('foo');

        $this->mock->fromObject($model);
    }
}

/**
 * @internal
 */
class ModelTest
{
    public function __construct(private string $id, private string $value)
    {
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function toArray(): array
    {
        return [
            'id'  => $this->id,
            'foo' => $this->value,
            'bar' => 'bar',
        ];
    }
}

/**
 * @internal
 */
class WrongModelTest
{
    public function __construct(private string $value)
    {
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
