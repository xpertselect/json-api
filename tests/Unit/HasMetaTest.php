<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Tests\TestCase;
use XpertSelect\JsonApi\HasMeta;

/**
 * @internal
 */
final class HasMetaTest extends TestCase
{
    private $mock;

    public function setUp(): void
    {
        $this->mock = new class () {
            use HasMeta;
        };
    }

    public function testAddMeta(): void
    {
        $this->mock->addMeta('a', 'value');

        $this->assertEquals('value', $this->mock->getMetaContainer()['a']);
    }

    public function testSetMeta(): void
    {
        $this->mock->setMetaContainer(['a' => 'value', 'b' => 'foo']);

        $this->assertEquals(['a' => 'value', 'b' => 'foo'], $this->mock->getMetaContainer());
    }

    public function testOverwriteMeta(): void
    {
        $this->mock->setMetaContainer(['a' => 'value', 'b' => 'foo']);
        $this->mock->addMeta('a', 'bar');

        $this->assertEquals(['a' => 'bar', 'b' => 'foo'], $this->mock->getMetaContainer());
    }
}
