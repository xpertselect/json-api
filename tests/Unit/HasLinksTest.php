<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Tests\TestCase;
use XpertSelect\JsonApi\HasLinks;

/**
 * @internal
 */
final class HasLinksTest extends TestCase
{
    private $mock;

    public function setUp(): void
    {
        $this->mock = new class () {
            use HasLinks;
        };
    }

    public function testAddLink(): void
    {
        $this->mock->addLink('a', 'value');

        $this->assertEquals('value', $this->mock->getLinkContainer()['a']);
    }

    public function testAddLinks(): void
    {
        $this->mock->setLinkContainer(['a' => 'value', 'b' => 'foo']);

        $this->assertEquals(['a' => 'value', 'b' => 'foo'], $this->mock->getLinkContainer());
    }

    public function testOverwriteLink(): void
    {
        $this->mock->setLinkContainer(['a' => 'value', 'b' => 'foo']);
        $this->mock->addLink('a', 'bar');

        $this->assertEquals(['a' => 'bar', 'b' => 'foo'], $this->mock->getLinkContainer());
    }
}
