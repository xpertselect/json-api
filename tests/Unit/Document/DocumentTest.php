<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Document;

use Tests\TestCase;
use XpertSelect\JsonApi\Document\ResourceDocument;
use XpertSelect\JsonApi\Serializer\BaseSerializer;
use XpertSelect\JsonApi\Serializer\SerializerInterface;

/**
 * @internal
 */
final class DocumentTest extends TestCase
{
    private SerializerInterface $serializer;

    public function setUp(): void
    {
        $this->serializer   = new class () extends BaseSerializer {
            public function getType(): string
            {
                return 'post';
            }

            public function getAttributes(array $model): array
            {
                return ['foo' => $model['foo']];
            }

            public function getIdentifier(array $model): string
            {
                return $model['id'];
            }
        };
    }

    public function testCreateDocument(): void
    {
        $data     = [
            'foo' => 'bar',
            'id'  => '1',
        ];

        $resource = $this->serializer->fromObject($data);
        $document = new ResourceDocument($resource);

        $document->addLink('link', 'https://example.com');
        $document->addMeta('rows', '1');

        $expected = [
            'jsonapi' => [
                'version' => '1.0',
            ],
            'meta' => [
                'rows' => '1',
            ],
            'links' => [
                'link' => 'https://example.com',
            ],
            'data' => [
                'type'       => 'post',
                'id'         => '1',
                'attributes' => [
                    'foo' => 'bar',
                ],
            ],
        ];
        $this->assertEquals($expected, $document->jsonSerialize());
    }
}
