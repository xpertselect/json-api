<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Document;

use Tests\TestCase;
use XpertSelect\JsonApi\Document\ResourceDocument;

/**
 * @internal
 */
final class ResourceDocumentTest extends TestCase
{
    public function testDataIsNullWhenNoResourceIsIncluded(): void
    {
        $document = new ResourceDocument();

        $this->assertNull($document->toArray()['data']);
    }
}
