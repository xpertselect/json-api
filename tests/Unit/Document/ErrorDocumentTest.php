<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Document;

use Tests\TestCase;
use XpertSelect\JsonApi\Document\ErrorDocument;

/**
 * @internal
 */
final class ErrorDocumentTest extends TestCase
{
    public function testCreateErrorDocument(): void
    {
        $expected = [
            'jsonapi' => [
                'version' => '1.0',
            ],
            'meta' => [
                'rows' => '1',
            ],
            'links' => [
                'link' => 'https://example.com',
            ],
            'errors' => [
                ['foo' => 'bar'],
                ['oof' => 'rab'],
            ],
        ];

        $this->assertEquals($expected, (new ErrorDocument())
            ->setMetaContainer(['rows' => '1'])
            ->setLinkContainer(['link' => 'https://example.com'])
            ->setErrors([
                ['foo' => 'bar'],
                ['oof' => 'rab'],
            ])->jsonSerialize());
    }

    public function testErrorsCanBeAdded(): void
    {
        $document = new ErrorDocument();

        $this->assertCount(0, $document->getErrors());
        $document->addError(['message' => 'foo']);
        $this->assertCount(1, $document->getErrors());
    }
}
