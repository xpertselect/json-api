<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Document;

use Tests\TestCase;
use XpertSelect\JsonApi\Document\CollectionDocument;
use XpertSelect\JsonApi\Resource\JsonApiCollection;
use XpertSelect\JsonApi\Serializer\BaseSerializer;
use XpertSelect\JsonApi\Serializer\SerializerInterface;

/**
 * @internal
 */
final class CollectionDocumentTest extends TestCase
{
    private SerializerInterface $serializer;

    public function setUp(): void
    {
        $this->serializer = new class () extends BaseSerializer {
            public function getType(): string
            {
                return 'post';
            }

            public function getAttributes(array $model): array
            {
                return ['foo' => $model['foo']];
            }

            public function getIdentifier(array $model): string
            {
                return $model['id'];
            }
        };
    }

    public function testCollectionDocument(): void
    {
        $resources1 = [];
        $data       = [[
            'foo' => 'bar',
            'id'  => '1',
        ], [
            'foo' => 'baz',
            'id'  => '2',
        ]];
        foreach ($data as $object) {
            $resources1[] = $this->serializer->fromObject($object);
        }

        $data2 = [[
            'foo' => 'bar',
            'id'  => '3',
        ], [
            'foo' => 'baz',
            'id'  => '4',
        ]];

        $collection = new JsonApiCollection($resources1);
        $resources  = [];
        foreach ($data2 as $resource) {
            $resources[] = $this->serializer->fromObject($resource);
        }
        $document = new CollectionDocument($collection);
        $document->addResources($resources)
            ->addLink('self', 'https://example.com')
            ->addMeta('itemCount', '4');

        $expected = [
            'jsonapi' => [
                'version' => '1.0',
            ],
            'links' => [
                'self' => 'https://example.com',
            ],
            'meta' => [
                'itemCount' => '4',
            ],
            'data' => [[
                'id'         => '1',
                'type'       => 'post',
                'attributes' => [
                    'foo' => 'bar',
                ], ], [
                    'id'         => '2',
                    'type'       => 'post',
                    'attributes' => [
                        'foo' => 'baz',
                    ],
                ], [
                    'id'         => '3',
                    'type'       => 'post',
                    'attributes' => [
                        'foo' => 'bar',
                    ],
                ], [
                    'id'         => '4',
                    'type'       => 'post',
                    'attributes' => [
                        'foo' => 'baz',
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $document->toArray());
    }
}
