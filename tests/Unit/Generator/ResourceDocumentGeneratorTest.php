<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Generator;

use RuntimeException;
use Tests\HasSerializers;
use Tests\TestCase;
use XpertSelect\JsonApi\Generator\ResourceDocumentGenerator;
use XpertSelect\JsonApi\Serializer\BaseSerializer;

/**
 * @internal
 */
final class ResourceDocumentGeneratorTest extends TestCase
{
    use HasSerializers;

    private BaseSerializer $serializer;

    private ResourceDocumentGenerator $sut;

    public function setUp(): void
    {
        $this->serializer = $this->simpleSerializer();

        $this->sut = new ResourceDocumentGenerator();
    }

    public function testGenerateDocument(): void
    {
        $resource = [
            'foo' => 'bar',
            'id'  => '1',
        ];

        $document = $this->sut->setResource($resource, $this->serializer)
            ->asDocument();

        $expected = [
            'jsonapi' => [
                'version' => '1.0',
            ],
            'data' => [
                'type'       => 'post',
                'id'         => '1',
                'attributes' => [
                    'foo' => 'bar',
                ],
            ],
        ];

        $this->assertEquals($expected, $document->toArray());
    }

    public function testGenerateDocumentThrowsException(): void
    {
        $this->expectException(RuntimeException::class);

        $this->sut->asDocument();
    }
}
