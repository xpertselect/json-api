<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Generator;

use Tests\HasSerializers;
use Tests\TestCase;
use XpertSelect\JsonApi\Generator\CollectionDocumentGenerator;
use XpertSelect\JsonApi\Serializer\BaseSerializer;

/**
 * @internal
 */
final class CollectionDocumentGeneratorTest extends TestCase
{
    use HasSerializers;

    private BaseSerializer $serializer;

    private CollectionDocumentGenerator $sut;

    public function setUp(): void
    {
        $this->serializer = $this->simpleSerializer();

        $this->sut = new CollectionDocumentGenerator();
    }

    public function testGenerateDocument(): void
    {
        $resources = [[
            'foo' => 'bar',
            'id'  => '1',
        ], [
            'foo' => 'baz',
            'id'  => '2',
        ],
        ];

        $document = $this->sut->addResources($resources, $this->serializer)->asDocument();
        $expected = [
            'jsonapi' => [
                'version' => '1.0',
            ],
            'data' => [[
                'id'         => '1',
                'type'       => 'post',
                'attributes' => [
                    'foo' => 'bar',
                ],
            ], [
                'id'         => '2',
                'type'       => 'post',
                'attributes' => [
                    'foo' => 'baz',
                ],
            ],
            ],
        ];
        $this->assertEquals($expected, $document->toArray());
    }
}
