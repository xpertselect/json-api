<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Generator;

use Tests\TestCase;
use XpertSelect\JsonApi\Generator\ErrorDocumentGenerator;

/**
 * @internal
 */
final class ErrorDocumentGeneratorTest extends TestCase
{
    private ErrorDocumentGenerator $sut;

    public function setUp(): void
    {
        $this->sut = new ErrorDocumentGenerator();
    }

    public function testGenerateDocument(): void
    {
        $errors = [
            ['title' => 'error message'],
            ['title' => 'another message'],
        ];

        $document = $this->sut->addErrors($errors)->asDocument();

        $expected = [
            'jsonapi' => [
                'version' => '1.0',
            ],
            'errors' => [
                ['title' => 'error message'],
                ['title' => 'another message'],
            ],
        ];

        $this->assertEquals($expected, $document->toArray());
    }
}
