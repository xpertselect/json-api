<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Tests\TestCase;
use XpertSelect\JsonApi\Relationship;
use XpertSelect\JsonApi\Resource\JsonApiResource;

/**
 * @internal
 */
final class RelationshipTest extends TestCase
{
    public function testToArrayIncludesResourceIdentification(): void
    {
        $relationship = new Relationship(new JsonApiResource('foo', 'bar'));

        $this->assertEquals([
            'data' => [
                'type' => 'foo',
                'id'   => 'bar',
            ],
        ], $relationship->toArray());
    }
}
