<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Resource;

use Tests\TestCase;
use XpertSelect\JsonApi\Resource\JsonApiCollection;
use XpertSelect\JsonApi\Serializer\BaseSerializer;
use XpertSelect\JsonApi\Serializer\SerializerInterface;

/**
 * @internal
 */
final class JsonCollectionTest extends TestCase
{
    private SerializerInterface $serializer;

    public function setUp(): void
    {
        $this->serializer = new class () extends BaseSerializer {
            public function getType(): string
            {
                return 'simple';
            }

            public function getAttributes(array $model): array
            {
                return [
                    'foo' => $model['foo'],
                ];
            }

            public function getIdentifier(array $model): string
            {
                return $model['id'];
            }
        };
    }

    public function testJsonCollectionGetFullObject(): void
    {
        $collection = $this->createCollection();

        $expected = [[
            'id'         => '1',
            'type'       => 'simple',
            'attributes' => [
                'foo' => 'bar',
            ],
        ], [
            'id'         => '2',
            'type'       => 'simple',
            'attributes' => [
                'foo' => 'baz',
            ], ], [
                'id'         => '3',
                'type'       => 'simple',
                'attributes' => [
                    'foo' => 'bar',
                ],
            ], [
                'id'         => '4',
                'type'       => 'simple',
                'attributes' => [
                    'foo' => 'baz',
                ],
            ]];
        $this->assertEquals($expected, $collection->toarray());
    }

    public function testJsonCollectionGetIdentifications(): void
    {
        $collection = $this->createCollection();

        $expected = [[
            'id'   => '1',
            'type' => 'simple',
        ], [
            'id'   => '2',
            'type' => 'simple', ],
            [
                'id'   => '3',
                'type' => 'simple',
            ], [
                'id'   => '4',
                'type' => 'simple',
            ], ];
        $this->assertEquals($expected, $collection->getIdentifications());
    }

    private function createCollection(): JsonApiCollection
    {
        $resources1 = [];
        $data       = [[
            'foo' => 'bar',
            'id'  => '1',
        ], [
            'foo' => 'baz',
            'id'  => '2',
        ]];
        foreach ($data as $object) {
            $resources1[] = $this->serializer->fromObject($object);
        }

        $data2 = [[
            'foo' => 'bar',
            'id'  => '3',
        ], [
            'foo' => 'baz',
            'id'  => '4',
        ]];

        $collection = new JsonApiCollection($resources1);
        $resources  = [];
        foreach ($data2 as $resource) {
            $resources[] = $this->serializer->fromObject($resource);
        }
        $collection->addResources($resources);

        return $collection;
    }
}
