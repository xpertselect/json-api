<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Resource;

use Tests\TestCase;
use XpertSelect\JsonApi\Serializer\BaseSerializer;
use XpertSelect\JsonApi\Serializer\SerializerInterface;

/**
 * @internal
 */
final class JsonResourceTest extends TestCase
{
    private SerializerInterface $objectSerializer;

    private SerializerInterface $simpleSerializer;

    public function setUp(): void
    {
        $this->objectSerializer = new class () extends BaseSerializer {
            public function getType(): string
            {
                return 'dataset';
            }

            public function getAttributes($model): array
            {
                return [
                    'a' => $model['foo'],
                    'b' => $model['bar'],
                    'c' => $model['baz'],
                ];
            }

            public function getIdentifier(array $model): string
            {
                return $model['id'];
            }

            public function getRelationships(array $model): array
            {
                return $model['relationship'];
            }
        };

        $this->simpleSerializer = new class () extends BaseSerializer {
            public function getType(): string
            {
                return 'simple';
            }

            public function getAttributes(array $model): array
            {
                return [];
            }

            public function getIdentifier(array $model): string
            {
                return $model['id'];
            }
        };
    }

    public function testCreateResource(): void
    {
        $model = [
            'foo'          => 'oof',
            'bar'          => 'rab',
            'baz'          => 'zab',
            'id'           => '1',
            'relationship' => [],
        ];

        $resource = $this->objectSerializer->fromObject($model)
            ->addLink('link', 'https://example.com')
            ->addMeta('rows', '1');

        $expected = [
            'type'       => 'dataset',
            'id'         => '1',
            'attributes' => [
                'a' => 'oof',
                'b' => 'rab',
                'c' => 'zab',
            ],
            'meta' => [
                'rows' => '1',
            ],
            'links' => [
                'link' => 'https://example.com',
            ],
        ];
        $this->assertEquals($expected, $resource->toArray());
    }

    public function testCreateSimpleResource(): void
    {
        $model = [
            'id'  => '1',
        ];

        $resource = $this->simpleSerializer->fromObject($model);
        $resource->addAttribute('foo', 'bar');

        $expected = [
            'type'       => 'simple',
            'id'         => '1',
            'attributes' => [
                'foo' => 'bar',
            ],
        ];
        $this->assertEquals($expected, $resource->toArray());
    }
}
