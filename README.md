# XpertSelect / JSON API

[gitlab.com/xpertselect/json-api](https://gitlab.com/xpertselect/json-api)

XpertSelect package providing utilities for creating a REST api conforming to the JSON:API specification.

## License

View the `LICENSE.md` file for licensing details.

## Installation

Installation of [`xpertselect/json-api`](https://packagist.org/packages/xpertselect/json-api) is done via [Composer](https://getcomposer.org).

```shell
composer require xpertselect/json-api
```
