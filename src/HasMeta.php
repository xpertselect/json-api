<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi;

/**
 * Trait HasMeta.
 *
 * Enables an object to maintain a JSON:API meta container.
 */
trait HasMeta
{
    /**
     * The JSON:API meta container.
     *
     * @var array<string, mixed>
     */
    private array $metaContainer = [];

    /**
     * Get the meta container.
     *
     * @return array<string, mixed> The meta container, as it is currently set
     */
    public function getMetaContainer(): array
    {
        return $this->metaContainer;
    }

    /**
     * Replace the current meta container.
     *
     * @param array<string, mixed> $metaContainer The container that should replace the current
     *                                            container
     *
     * @return $this This instance, for method chaining
     */
    public function setMetaContainer(array $metaContainer): self
    {
        $this->metaContainer = $metaContainer;

        return $this;
    }

    /**
     * Add a value to the meta container.
     *
     * @param array<string, mixed>|string $value The value to add
     *
     * @return $this This instance, for method chaining
     */
    public function addMeta(string $key, array|string $value): self
    {
        $this->metaContainer[$key] = $value;

        return $this;
    }
}
