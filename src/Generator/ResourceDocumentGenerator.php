<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi\Generator;

use RuntimeException;
use XpertSelect\JsonApi\Document\DocumentInterface;
use XpertSelect\JsonApi\Document\ResourceDocument;
use XpertSelect\JsonApi\Resource\JsonApiResource;
use XpertSelect\JsonApi\Serializer\SerializerInterface;

/**
 * Class JsonResourceDocumentGenerator.
 *
 * Generator responsible for creating JSON:API documents that represent a single JSON:API resource.
 */
class ResourceDocumentGenerator implements DocumentGeneratorInterface
{
    /**
     * The JSON:API resource to generate the document for.
     */
    private ?JsonApiResource $resource = null;

    /**
     * Creates a JSON:API resource from the data with the serializer and assigns it to the document
     * being constructed.
     *
     * @param object|array<string, mixed> $data       The data to transform into a JSON:API resource
     * @param SerializerInterface         $serializer The service that transforms the data
     *
     * @return $this This instance, for method chaining
     */
    public function setResource(array|object $data, SerializerInterface $serializer): DocumentGeneratorInterface
    {
        $this->resource = $serializer->fromObject($data);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function asDocument(): DocumentInterface
    {
        if (null === $this->resource) {
            throw new RuntimeException('Resource cannot be null');
        }

        return new ResourceDocument($this->resource);
    }
}
