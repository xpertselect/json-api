<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi\Generator;

use XpertSelect\JsonApi\Document\DocumentInterface;

/**
 * Interface DocumentGeneratorInterface.
 *
 * Implementation contract for generating JSON:API documents.
 */
interface DocumentGeneratorInterface
{
    /**
     * Return the generated document.
     *
     * @return DocumentInterface The generated JSON:API document
     */
    public function asDocument(): DocumentInterface;
}
