<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi\Generator;

use XpertSelect\JsonApi\Document\CollectionDocument;
use XpertSelect\JsonApi\Document\DocumentInterface;
use XpertSelect\JsonApi\Resource\JsonApiCollection;
use XpertSelect\JsonApi\Resource\JsonApiResource;
use XpertSelect\JsonApi\Serializer\SerializerInterface;

/**
 * Class JsonCollectionDocumentGenerator.
 *
 * Implementation of the DocumentGeneratorInterface whose sole purpose is to generate JSON documents
 * that contain a collection of resources as specified by the JSON API specification.
 */
class CollectionDocumentGenerator implements DocumentGeneratorInterface
{
    /**
     * The JSON resources that will become part of the generated JSON document.
     *
     * @var JsonApiResource[]
     */
    private array $resources = [];

    /**
     * Add the given data as a single resource to the document.
     *
     * @param object|array<string, mixed> $data       The data to add as a resource to the document
     * @param SerializerInterface         $serializer The serializer to apply to the given data
     *
     * @return $this This instance, for method chaining
     */
    public function addResource(array|object $data,
                                SerializerInterface $serializer): DocumentGeneratorInterface
    {
        $this->resources[] = $serializer->fromObject($data);

        return $this;
    }

    /**
     * Adds an array of data as resources to the JSON document.
     *
     * @param object[]|array<int, array<string, mixed>> $data       The data to add as resources to
     *                                                              the document
     * @param SerializerInterface                       $serializer The serializer to apply to the
     *                                                              given data
     *
     * @return $this This instance, for method chaining
     */
    public function addResources(array $data,
                                 SerializerInterface $serializer): DocumentGeneratorInterface
    {
        foreach ($data as $object) {
            $this->addResource($object, $serializer);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function asDocument(): DocumentInterface
    {
        return new CollectionDocument(new JsonApiCollection($this->resources));
    }
}
