<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi\Generator;

use XpertSelect\JsonApi\Document\DocumentInterface;
use XpertSelect\JsonApi\Document\ErrorDocument;

/**
 * Class ErrorDocumentGenerator.
 *
 * Generator responsible for creating JSON:API error responses.
 */
class ErrorDocumentGenerator implements DocumentGeneratorInterface
{
    /**
     * The errors to communicate.
     *
     * @var array<int, array<string, mixed>>
     */
    private array $errors = [];

    /**
     * Add an error to the list of errors.
     *
     * @param array<string, mixed> $error The error that should be included in the document
     *
     * @return $this This instance, for method chaining
     */
    public function addError(array $error): self
    {
        $this->errors[] = $error;

        return $this;
    }

    /**
     * Add an array of errors to the list.
     *
     * @param array<int, array<string, mixed>> $errors The errors to include in the document
     *
     * @return $this This instance, for method chaining
     */
    public function addErrors(array $errors): self
    {
        foreach ($errors as $error) {
            $this->addError($error);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function asDocument(): DocumentInterface
    {
        $document = new ErrorDocument();
        $document->setErrors($this->errors);

        return $document;
    }
}
