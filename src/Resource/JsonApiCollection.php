<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi\Resource;

/**
 * Class JsonApiCollection.
 *
 * Represents a collection of JSON:API resources.
 */
final class JsonApiCollection
{
    /**
     * JsonApiCollection constructor.
     *
     * @param JsonApiResource[] $resources The JSON:API resources in the collection
     */
    public function __construct(private array $resources = [])
    {
    }

    /**
     * Add a JSON:API resource to the collection.
     *
     * @param JsonApiResource $resource The JSON:API resource to add to the collection
     *
     * @return $this This instance, for method chaining
     */
    public function addResource(JsonApiResource $resource): self
    {
        $this->resources[] = $resource;

        return $this;
    }

    /**
     * Add a list of JSON:API resources to the collection.
     *
     * @param JsonApiResource[] $resources The JSON:API resources to add to the collection
     *
     * @return $this This instance, for method chaining
     */
    public function addResources(array $resources): self
    {
        foreach ($resources as $resource) {
            $this->addResource($resource);
        }

        return $this;
    }

    /**
     * Get an array with all the JSON:API Identifications of the resources in the collection.
     *
     * @return array<int, array<string, mixed>> The JSON:API identifications of the underlying
     *                                          JSON:API resources
     */
    public function getIdentifications(): array
    {
        return array_map(function(JsonApiResource $resource) {
            return $resource->getIdentification();
        }, $this->resources);
    }

    /**
     * Create an array representation of this JsonApiCollection.
     *
     * @return array<int, array<string, mixed>> The array representation of the collection
     */
    public function toArray(): array
    {
        return array_map(function(JsonApiResource $resource) {
            return $resource->toArray();
        }, $this->resources);
    }
}
