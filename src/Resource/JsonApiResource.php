<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi\Resource;

use XpertSelect\JsonApi\HasLinks;
use XpertSelect\JsonApi\HasMeta;
use XpertSelect\JsonApi\Relationship;

/**
 * Class JsonApiResource.
 *
 * Represents a single JSON:API resource.
 */
class JsonApiResource
{
    use HasLinks;
    use HasMeta;

    /**
     * Represents the attributes that belong to this resource.
     *
     * @var array<string, mixed> The attributes of the resource
     */
    private array $attributes = [];

    /**
     * Represents the relations of this resource.
     *
     * @var Relationship[] The relations of the resource
     */
    private array $relationships = [];

    /**
     * Create a new JsonApiResource instance from the given data.
     *
     * @return JsonApiResource The created instance
     */
    public static function create(string $type, string $identifier): self
    {
        return new self($type, $identifier);
    }

    /**
     * JsonApiResource constructor.
     *
     * @param string $type       The type of the resource
     * @param string $identifier The ID of the resource
     */
    final public function __construct(private readonly string $type,
                                      private readonly string $identifier)
    {
    }

    /**
     * Get the identifier of the resource.
     *
     * @return string The identifier
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * Get the type of the resource.
     *
     * @return string The type
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Set the attributes of this resource.
     *
     * @param array<string, mixed> $attributes The attributes to set
     *
     * @return $this This instance, for method chaining
     */
    public function setAttributes(array $attributes): self
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * Add an attribute to the resource.
     *
     * @param string $key   The name of the attribute
     * @param mixed  $value The value of the attribute
     *
     * @return $this This instance, for method chaining
     */
    public function addAttribute(string $key, mixed $value): self
    {
        $this->attributes[$key] = $value;

        return $this;
    }

    /**
     * Set the relationships of this resource.
     *
     * @param Relationship[] $relationships The relationships to set
     *
     * @return $this This instance, for method chaining
     */
    public function setRelationships(array $relationships): self
    {
        $this->relationships = $relationships;

        return $this;
    }

    /**
     * Add a relationship of this resource.
     *
     * @param Relationship $relation The relationship to add
     *
     * @return $this This instance, for method chaining
     */
    public function addRelationship(Relationship $relation): self
    {
        $this->relationships[] = $relation;

        return $this;
    }

    /**
     * Retrieve any resources that may be included as top-level included resources.
     *
     * @return JsonApiResource[] The resources that may be included
     */
    public function getIncludedResources(): array
    {
        return array_map(function(Relationship $relationship) {
            return $relationship->resource;
        }, array_filter($this->relationships, function(Relationship $relationship) {
            return $relationship->includeInDocument;
        }));
    }

    /**
     * Get the identification part of the resource.
     *
     * @return array{type: string, id: string} The JSON:API identifying information
     */
    public function getIdentification(): array
    {
        return [
            'type' => $this->getType(),
            'id'   => $this->getIdentifier(),
        ];
    }

    /**
     * Get the resource in its JSON:API representation.
     *
     * @return array<string, mixed> The JSON:API representation of the resource
     */
    public function toArray(): array
    {
        $document = $this->getIdentification();

        if (!empty($this->attributes)) {
            $document['attributes'] = $this->attributes;
        }

        if (method_exists($this, 'getLinkContainer') && ($links = $this->getLinkContainer())) {
            $document['links'] = $links;
        }

        if (!empty($this->relationships)) {
            foreach ($this->relationships as $relationship) {
                $document['relationships'][] = $relationship->toArray();
            }
        }

        if (method_exists($this, 'getMetaContainer') && ($meta = $this->getMetaContainer())) {
            $document['meta'] = $meta;
        }

        return $document;
    }
}
