<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi\Serializer;

use RuntimeException;
use XpertSelect\JsonApi\Resource\JsonApiResource;

/**
 * Class BaseSerializer.
 *
 * This class provides the base functionality that all serializers need in order to do their job.
 */
abstract class BaseSerializer implements SerializerInterface
{
    /**
     * {@inheritdoc}
     */
    public function fromObject(array|object $object): JsonApiResource
    {
        if (is_object($object)) {
            if (method_exists($object, 'toArray')) {
                $object = $object->toArray();
            } else {
                throw new RuntimeException('Object must have an toArray method');
            }
        }

        return JsonApiResource::create($this->getType(), $this->getIdentifier($object))
            ->setAttributes($this->getAttributes($object))
            ->setLinkContainer($this->getLinks($object))
            ->setMetaContainer($this->getMeta($object))
            ->setRelationships($this->getRelationships($object));
    }

    /**
     * {@inheritdoc}
     */
    public function getRelationships(array $model): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getLinks(array $model): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getMeta(array $model): array
    {
        return [];
    }
}
