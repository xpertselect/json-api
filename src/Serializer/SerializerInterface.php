<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi\Serializer;

use XpertSelect\JsonApi\Relationship;
use XpertSelect\JsonApi\Resource\JsonApiResource;

/**
 * Interface SerializerInterface.
 *
 * Implementation contract for classes that transform data into JSON:API resources.
 */
interface SerializerInterface
{
    /**
     * Retrieve the type of resources this serializer generated.
     *
     * @return string The type of resources that this serializer creates
     */
    public function getType(): string;

    /**
     * Generate a JSON:API resource from the given data.
     *
     * @param array<string, mixed>|object $object The data to generate a resource from
     *
     * @return JsonApiResource The generated JSON:API resource
     */
    public function fromObject(array|object $object): JsonApiResource;

    /**
     * Retrieve the JSON:API attributes for the given model.
     *
     * @param array<string, mixed> $model The model to retrieve the JSON:API attributes for
     *
     * @return array<string, mixed> The JSON:API attributes
     */
    public function getAttributes(array $model): array;

    /**
     * Retrieve the JSON:API relations for the given model.
     *
     * @param array<string, mixed> $model The model to retrieve the JSON:API relations for
     *
     * @return Relationship[] The JSON:API relations
     */
    public function getRelationships(array $model): array;

    /**
     * Retrieve the JSON:API identifier of the given model.
     *
     * @param array<string, mixed> $model The model to retrieve the JSON:API identifier for
     *
     * @return string The JSON:API identifier for the given model
     */
    public function getIdentifier(array $model): string;

    /**
     * Get the JSON:API links of the given model.
     *
     * @param array<string, mixed> $model The model to retrieve the JSON:API links for
     *
     * @return array<string, mixed> The JSON:API links for the model
     */
    public function getLinks(array $model): array;

    /**
     * Get the JSON:API meta keys of the given model.
     *
     * @param array<string, mixed> $model The model to retrieve the JSON:API meta information for
     *
     * @return array<string, mixed> The JSON:API meta information for the model
     */
    public function getMeta(array $model): array;
}
