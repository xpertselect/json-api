<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi;

/**
 * Trait HasLinks.
 *
 * Enables an object to maintain a JSON:API link container.
 */
trait HasLinks
{
    /**
     * The JSON:API link container.
     *
     * @var array<string, mixed>
     */
    private array $linkContainer = [];

    /**
     * Get the link container.
     *
     * @return array<string, mixed> The link container, as it is currently set
     */
    public function getLinkContainer(): array
    {
        return $this->linkContainer;
    }

    /**
     * Replace the current link container.
     *
     * @param array<string, mixed> $linkContainer The container that should replace the current
     *                                            container
     *
     * @return $this This instance, for method chaining
     */
    public function setLinkContainer(array $linkContainer): self
    {
        $this->linkContainer = $linkContainer;

        return $this;
    }

    /**
     * Add a new ink to the container. Will overwrite if the given key already exists.
     *
     * @param string|array<int|string, mixed> $value The value to add
     *
     * @return $this This instance, for method chaining
     */
    public function addLink(string $key, string|array $value): self
    {
        $this->linkContainer[$key] = $value;

        return $this;
    }
}
