<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use XpertSelect\JsonApi\Document\DocumentInterface;
use XpertSelect\JsonApi\Generator\ErrorDocumentGenerator;

/**
 * Trait JsonApiResponseUtilities.
 *
 * Trait for creating reusable HTTP JSON:API response objects.
 */
trait JsonApiResponseUtilities
{
    /**
     * Creates a JSON:API response from the given document.
     *
     * @param null|DocumentInterface $document The document to create a response for
     * @param int                    $status   The HTTP status code to assign to the response
     * @param array<string, string>  $headers  Any additional HTTP headers to append to the response
     */
    public function asJsonApiResponse(?DocumentInterface $document = null, int $status = 200,
                                      array $headers = []): JsonResponse
    {
        $data = $document ? $document->toArray() : [];

        return new JsonResponse($data, $status, array_merge($this->jsonApiHeaders(), $headers));
    }

    /**
     * Create a No Content JsonResponse object.
     *
     * @OA\Response(
     *   response="NoContentResponse",
     *   description="Indicates that the server has successfully fulfilled the request and that there is no content to send back.",
     * )
     *
     * @return Response The created HTTP response object
     */
    public function noContentResponse(): Response
    {
        return new Response(null, 204);
    }

    /**
     * Create Bad Request Error JsonResponse object.
     *
     * @OA\Response(
     *   response="BadRequestErrorResponse",
     *   description="Indicates a bad request was done by the client.",
     *
     *   @OA\MediaType(
     *     mediaType="application/vnd.api+json",
     *
     *     @OA\Schema(ref="#/components/schemas/BadRequestErrorDocument")
     *   )
     * )
     * @OA\Schema(
     *   schema="BadRequestErrorDocument",
     *
     *   @OA\Property(property="jsonapi", ref="#/components/schemas/JsonApiContainer"),
     *   @OA\Property(
     *     property="errors",
     *     type="array",
     *
     *     @OA\Items(
     *
     *       @OA\Property(
     *         property="status",
     *         type="string",
     *         example="400"
     *       ),
     *       @OA\Property(
     *         property="title",
     *         type="string",
     *         example="Bad Request Error"
     *       )
     *     )
     *   )
     * )
     *
     * @param array<int, array<string, mixed>> $errors An array of error objects
     *
     * @return JsonResponse The created HTTP response object
     */
    public function badRequestErrorResponse(array $errors): JsonResponse
    {
        $document = (new ErrorDocumentGenerator())
            ->addErrors(array_map(function($error): array {
                $error['status'] = '400';

                return $error;
            }, $errors))
            ->asDocument();

        return $this->asJsonApiResponse($document, 400);
    }

    /**
     * Create a No Access Error JsonResponse object.
     *
     * @OA\Response(
     *   response="NoAccessErrorResponse",
     *   description="Indicates that the user does not have sufficient permissions to perform this action.",
     *
     *   @OA\MediaType(
     *     mediaType="application/vnd.api+json",
     *
     *     @OA\Schema(ref="#/components/schemas/NoAccessErrorDocument")
     *   )
     * )
     * @OA\Schema(
     *   schema="NoAccessErrorDocument",
     *
     *   @OA\Property(property="jsonapi", ref="#/components/schemas/JsonApiContainer"),
     *   @OA\Property(
     *     property="errors",
     *     type="array",
     *
     *     @OA\Items(
     *
     *       @OA\Property(
     *         property="status",
     *         type="string",
     *         example="403"
     *       ),
     *       @OA\Property(
     *         property="title",
     *         type="string",
     *         example="No Access Error"
     *       )
     *     )
     *   )
     * )
     *
     * @return JsonResponse The created HTTP response object
     */
    public function noAccessErrorResponse(): JsonResponse
    {
        $document = (new ErrorDocumentGenerator())
            ->addError([
                'status' => '403',
                'title'  => 'You do not have sufficient permissions to perform this action',
            ])
            ->asDocument();

        return $this->asJsonApiResponse($document, 403);
    }

    /**
     * Create a Not Found Error JsonResponse object.
     *
     * @OA\Response(
     *   response="NotFoundErrorResponse",
     *   description="Indicates that the requested resource was not found.",
     *
     *   @OA\MediaType(
     *     mediaType="application/vnd.api+json",
     *
     *     @OA\Schema(ref="#/components/schemas/NotFoundErrorDocument")
     *   )
     * )
     * @OA\Schema(
     *   schema="NotFoundErrorDocument",
     *
     *   @OA\Property(property="jsonapi", ref="#/components/schemas/JsonApiContainer"),
     *   @OA\Property(
     *     property="errors",
     *     type="array",
     *
     *     @OA\Items(
     *
     *       @OA\Property(
     *         property="status",
     *         type="string",
     *         example="404"
     *       ),
     *       @OA\Property(
     *         property="title",
     *         type="string",
     *         example="Not Found Error"
     *       )
     *     )
     *   )
     * )
     *
     * @return JsonResponse The created HTTP response object
     */
    public function notFoundErrorResponse(): JsonResponse
    {
        $document = (new ErrorDocumentGenerator())
            ->addError([
                'status' => '404',
                'title'  => 'The requested resource was not found',
            ])
            ->asDocument();

        return $this->asJsonApiResponse($document, 404);
    }

    /**
     * Create an Internal Server Error JsonResponse object.
     *
     * @OA\Response(
     *   response="InternalServerErrorResponse",
     *   description="Indicates an unrecoverable internal error occured while processing the HTTP request.",
     *
     *   @OA\MediaType(
     *     mediaType="application/vnd.api+json",
     *
     *     @OA\Schema(ref="#/components/schemas/InternalServerErrorDocument")
     *   )
     * )
     * @OA\Schema(
     *   schema="InternalServerErrorDocument",
     *
     *   @OA\Property(property="jsonapi", ref="#/components/schemas/JsonApiContainer"),
     *   @OA\Property(
     *     property="errors",
     *     type="array",
     *
     *     @OA\Items(
     *
     *       @OA\Property(
     *         property="status",
     *         type="string",
     *         example="500"
     *       ),
     *       @OA\Property(
     *         property="title",
     *         type="string",
     *         example="Internal Server Error"
     *       )
     *     )
     *   )
     * )
     *
     * @return JsonResponse The created HTTP response object
     */
    public function internalServerErrorResponse(): JsonResponse
    {
        $document = (new ErrorDocumentGenerator())
            ->addError([
                'status' => '500',
                'title'  => 'Internal Server Error',
            ])
            ->asDocument();

        return $this->asJsonApiResponse($document, 500);
    }

    /**
     * Create a Conflict Error JsonResponse object.
     *
     * @OA\Response(
     *   response="ConflictErrorResponse",
     *   description="Conflict. Given when the operation cannot be executed because a resource is in a conflicting state. For example, a resource exists when it should not, or vice versa.",
     *
     *   @OA\MediaType(
     *     mediaType="application/vnd.api+json",
     *
     *     @OA\Schema(ref="#/components/schemas/ConflictErrorDocument")
     *   )
     * )
     * @OA\Schema(
     *   schema="ConflictErrorDocument",
     *
     *   @OA\Property(property="jsonapi", ref="#/components/schemas/JsonApiContainer"),
     *   @OA\Property(
     *     property="errors",
     *     type="array",
     *
     *     @OA\Items(
     *
     *       @OA\Property(
     *         property="status",
     *         type="string",
     *         example="409"
     *       ),
     *       @OA\Property(
     *         property="title",
     *         type="string",
     *         example="Request to delete content-type that does not exist."
     *       )
     *     )
     *   )
     * )
     *
     * @param string $message The message to return
     *
     * @return JsonResponse The created HTTP response object
     */
    public function conflictErrorResponse(string $message): JsonResponse
    {
        $document = (new ErrorDocumentGenerator())
            ->addError([
                'status' => '409',
                'title'  => $message,
            ])
            ->asDocument();

        return $this->asJsonApiResponse($document, 409);
    }

    /**
     * Create a Unauthenticated Error JsonResponse object.
     *
     * @OA\Response(
     *   response="UnauthenticatedErrorResponse",
     *   description="Unauthenticated.",
     *
     *   @OA\MediaType(
     *     mediaType="application/vnd.api+json",
     *
     *     @OA\Schema(ref="#/components/schemas/UnauthenticatedErrorDocument")
     *   )
     * )
     * @OA\Schema(
     *   schema="UnauthenticatedErrorDocument",
     *
     *   @OA\Property(property="jsonapi", ref="#/components/schemas/JsonApiContainer"),
     *   @OA\Property(
     *     property="errors",
     *     type="array",
     *
     *     @OA\Items(
     *
     *       @OA\Property(
     *         property="status",
     *         type="string",
     *         example="401"
     *       ),
     *       @OA\Property(
     *         property="title",
     *         type="string",
     *         example="Unauthenticated."
     *       )
     *     )
     *   )
     * )
     *
     * @return JsonResponse The created HTTP response object
     */
    public function unauthenticatedErrorResponse(): JsonResponse
    {
        $document = (new ErrorDocumentGenerator())
            ->addError([
                'status' => '401',
                'title'  => 'Unauthenticated.',
            ])
            ->asDocument();

        return $this->asJsonApiResponse($document, 401);
    }

    /**
     * Returns the base HTTP headers for each JSON:API response.
     *
     * @return array<string, string> The headers for each JSON:API response
     */
    private function jsonApiHeaders(): array
    {
        return [
            'Content-Type' => 'application/vnd.api+json',
        ];
    }
}
