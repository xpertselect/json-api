<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi;

use XpertSelect\JsonApi\Resource\JsonApiResource;

/**
 * Class Relationship.
 *
 * Defines a relationship to a JsonApiResource.
 */
final class Relationship
{
    /**
     * Relationship constructor.
     *
     * @param JsonApiResource $resource          The resource that is related
     * @param bool            $includeInDocument Whether the related resource may be included as an
     *                                           included resource in the top-level document
     */
    public function __construct(public readonly JsonApiResource $resource,
                                public readonly bool $includeInDocument = false)
    {
    }

    /**
     * Retrieve the array representation of this relationship.
     *
     * @return array{data: array{type: string, id: string}} The resource linkage
     */
    public function toArray(): array
    {
        return [
            'data' => $this->resource->getIdentification(),
        ];
    }
}
