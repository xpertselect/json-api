<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi\Document;

use XpertSelect\JsonApi\HasLinks;
use XpertSelect\JsonApi\HasMeta;

/**
 * Class BaseDocument.
 *
 * Base implementation of a JSON:API document.
 */
abstract class BaseDocument implements DocumentInterface
{
    use HasMeta;
    use HasLinks;

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize(): mixed
    {
        return $this->toArray();
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        $document = ['jsonapi' => ['version' => '1.0']];

        if ($meta = $this->getMetaContainer()) {
            $document['meta'] = $meta;
        }

        if ($links = $this->getLinkContainer()) {
            $document['links'] = $links;
        }

        return $document;
    }
}
