<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi\Document;

use XpertSelect\JsonApi\Resource\JsonApiResource;

/**
 * Class ResourceDocument.
 *
 * Represents a JSON:API document that holds a single JSON:API resource.
 */
final class ResourceDocument extends BaseDocument
{
    /**
     * ResourceDocument constructor.
     *
     * @param null|JsonApiResource $resource The JSON:API resource to include
     */
    public function __construct(private readonly ?JsonApiResource $resource = null)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        $documentData = [
            'data' => null,
        ];

        if ($this->resource) {
            $documentData['data'] = $this->resource->toArray();

            if ($included = $this->resource->getIncludedResources()) {
                foreach ($included as $resource) {
                    $documentData['included'][] = $resource->toArray();
                }
            }
        }

        return array_merge(parent::toArray(), $documentData);
    }
}
