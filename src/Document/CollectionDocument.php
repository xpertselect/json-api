<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi\Document;

use XpertSelect\JsonApi\Resource\JsonApiCollection;
use XpertSelect\JsonApi\Resource\JsonApiResource;

/**
 * @OA\Schema(
 *   schema="SearchResultsMeta",
 *
 *   @OA\Property(
 *     property="start",
 *     type="int",
 *     example=0
 *   ),
 *   @OA\Property(
 *     property="rows",
 *     type="int",
 *     example=10
 *   ),
 *   @OA\Property(
 *     property="total",
 *     type="int",
 *     example=100
 *   ),
 * )
 *
 * Class CollectionDocument.
 *
 * Represents a JSON:API document that holds a collection of JSON:API resources.
 */
final class CollectionDocument extends BaseDocument
{
    /**
     * CollectionDocument Constructor.
     *
     * @param JsonApiCollection $collection The collection to represent in a JSON:API document
     */
    public function __construct(private readonly JsonApiCollection $collection)
    {
    }

    /**
     * Add a single JSON:API resource to the collection of this JSON:API document.
     *
     * @param JsonApiResource $resource The resource to add to the collection
     *
     * @return $this This instance, for method chaining
     */
    public function addResource(JsonApiResource $resource): self
    {
        $this->collection->addResource($resource);

        return $this;
    }

    /**
     * Adds an array of JSON:API resources to the document.
     *
     * @param JsonApiResource[] $resources The resources to add to the collection
     *
     * @return $this This instance, for method chaining
     */
    public function addResources(array $resources): self
    {
        foreach ($resources as $resource) {
            $this->addResource($resource);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return array_merge(parent::toArray(), [
            'data' => $this->collection->toArray(),
        ]);
    }
}
