<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi\Document;

/**
 * Class ErrorDocument.
 *
 * Represents a JSON:API error document.
 */
final class ErrorDocument extends BaseDocument
{
    /**
     * The errors to include in the JSON:API document.
     *
     * @var array<int, array<string, mixed>> The JSON:API errors
     */
    private array $errors = [];

    /**
     * Return the JSON API errors.
     *
     * @return array<int, array<string, mixed>> The JSON:API errors
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Overwrites the errors array with the given value.
     *
     * @param array<int, array<string, mixed>> $errors The new JSON:API errors
     *
     * @return $this This instance, for method chaining
     */
    public function setErrors(array $errors): self
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * Add a new error to the error array.
     *
     * @param array<string, mixed> $value The JSON:API error to include
     *
     * @return $this This instance, for method chaining
     */
    public function addError(array $value): self
    {
        $this->errors[] = $value;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return array_merge(parent::toArray(), [
            'errors' => $this->getErrors(),
        ]);
    }
}
