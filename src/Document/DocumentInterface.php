<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/json-api package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\JsonApi\Document;

use JsonSerializable;

/**
 * @OA\Schema(
 *   schema="JsonApiContainer",
 *
 *   @OA\Property(
 *     property="version",
 *     readOnly=true,
 *     type="string",
 *     example="1.0"
 *   )
 * )
 *
 * Interface DocumentInterface.
 *
 * Implementation contract defining a JSON:API document.
 */
interface DocumentInterface extends JsonSerializable
{
    /**
     * Transform the JSON:API document into its array representation.
     *
     * @return array<string, mixed> The array representation of the document
     */
    public function toArray(): array;
}
